console.log("Hello world");

// Arithmetic operators section
let x = 1397;
let y = 7831;

let sum = x + y;
console.log(sum);

let difference = x - y;
console.log(difference);

let product = x * y;
console.log(product);

let quotient = x / y;
console.log(quotient);

let remainder = y % x;
console.log(remainder);

// Assignment Operator
let assignmentNumber = 8;
// addition assignment operator
// assignmentNumber = assignmentNumber + 2; 

// shorthand for addition assignment (+=)
assignmentNumber += 2;
console.log(assignmentNumber);

assignmentNumber -= 2;
console.log(assignmentNumber);

assignmentNumber *= 2;
console.log(assignmentNumber);

assignmentNumber /= 2;
console.log(assignmentNumber);

// Multiple operators and parenthesis

// When multiple operators are present in a single statement, it follows the PEMDAS
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * 4 / 5;
console.log(pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log(pemdas);

// Increment and decrement Section
let z = 1;
let increment = ++z;

// pre-increment
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Type Coercion
// It is the automatic or implicit conversion of values from one data type to another. This happens when operations are performed on different data types that would normally not be possible and yield irregular result.

// Values are automatically assigned/converted from one data type to another in order to resoble operation

let numbA = '10';
let numbB = 12;
let coercion = numbA + numbB;
console.log(coercion)

// mini activity

let numA = 1;
let numB = 1;
let coercion1 = numA + numB;
console.log("Coercion result of two number: " + coercion1);

let boolVar = true;
let numC = 2;
let coercion2 = Boolean(boolVar) + numC;
console.log("Coercion result of boolean and number: " + coercion2);

let boolVar2 = false;
let myString = "Saitama";
let coercion3 = boolVar2 + myString;
console.log("Coercion result of boolean and number: " + coercion3);

// Console operators

// Equality operator
console.log(1 == 1);
console.log(1 == 2);

console.log(1 == "1");
console.log(1 == true);
console.log("juan" == "juan");
let juan = 'juan';
console.log('juan' == juan);

// Inequality operator
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log("juan" != 'juan');
console.log('juan' != juan);

// Strict equality/inequality operator
// strict equality
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log("juan" === 'juan');
console.log('juan' === juan);

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan" !== 'juan');
console.log('juan' !== juan);

// Relational operators
// some comparison operators check wether one value is greater or less than to the other value

let a = 50;
let b = 65;

let greaterThan = a > b;
let lessThan = a < b;
let greaterThanOrEqualTo = a >= b;
let lessThanOrEqualTo = a <= b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

// true - product of a forced coercion to change the string into a number data type
let numStr = "30";
console.log(a > numStr);

// since a string is not numeric, the string was not converted into a number and it resulted into a NaN (Not a Number). 65 is not greater than NaN
let str = "twenty";
console.log(b >= str);

// Logical operators
// Checking whether the values of the two or more variables are true/false
let isLegalAge = true;
let isRegistered = false;

// And Operator ( && )

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of And Operator: " + allRequirementsMet);

// OR Operator ( || )

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of OR Operator: " + someRequirementsMet);

// NOT Operator

let someRequirementsNotMet = !isRegistered;
console.log("Result of NOT Operator: " + someRequirementsNotMet);